<?php
 //echo $_GET['edit_comment'];
require 'storyPavilion_database.php';
 $stmt = $mysqli->prepare("update storys set story_title = ? where story_id=?");

   if(!$stmt) {
    printf("Query Prep For My Stories Failed: %s\n", $mysqli->error);
   }
 
   $stmt->bind_param('si',$_GET['edit_story_title'],$_GET['story_id']);
   $stmt->execute();
   
   $stmt = $mysqli->prepare("update storys set story_description = ? where story_id=?");

   if(!$stmt) {
    printf("Query Prep For My Stories Failed: %s\n", $mysqli->error);
   }
 
   $stmt->bind_param('si',$_GET['edit_story_description'],$_GET['story_id']);
   $stmt->execute();
   
   $stmt = $mysqli->prepare("update storys set story_contents = ? where story_id=?");

   if(!$stmt) {
    printf("Query Prep For My Stories Failed: %s\n", $mysqli->error);
   }
 
   $stmt->bind_param('si',$_GET['edit_story_contents'],$_GET['story_id']);
   $stmt->execute();
   $stmt->close();
   header('Location:my_story.php');
?>