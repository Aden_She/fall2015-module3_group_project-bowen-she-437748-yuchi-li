<!doctype html>
 <html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="plain.css">
<style>
   #header {
    background-color:black;
    color:white;
    text-align:center;
    padding:5px;
}
#nav {
    line-height:30px;
    background-color:#eeeeee;
    height:900px;
    width:100px;
    float:left;
    padding:5px;	      
}
#section {
    width:350px;
    float:left;
    padding:10px;
    text-align: left;
}
#footer {
    background-color:black;
    color:white;
    clear:both;
    text-align:center;
   padding:5px;	 	 
}
#selected {
    color:grey;
}
#users{
   color:red;
}
.commentBlock {
   height : 100px;
   width: 200px;
   text-align: left;
}
</style>
    <title>Story Comment</title>
</head>

<div id="header">
    <h1>
       Story Comment
    </h1>
	<a href=logout.php>log out!</a>
</div>
<!-- add your wishing tab into your menu -->
<div id="nav">
    <a href=story_gallery.php>Story Gallery</a><br>
    <a href=my_story.php>My Story</a><br>
    <a href=my_comments.php>My Comments</a><br>
    <a href=new_story.php>New Story</a><br>
</div>



<?php
require 'storyPavilion_database.php';
session_start();

$user_id = $_SESSION['user_id'];
$story_id = $_GET['story_id'];
if(!$story_id) {
   printf("This story no longer exists");
}

$stmt = $mysqli->prepare("select story_title,story_description,story_contents,datetime,users.username from storys join users on (storys.user_id=users.user_id and storys.story_id=?)");
$stmt->bind_param('i',$story_id);
$stmt->execute();
$stmt->bind_result($story_title,$story_description,$story_contents,$datetime,$story_username);

$result = $stmt->get_result();

echo "<ul>\n";
while($row = $result->fetch_assoc()) {
    printf("
            <div id= >
                        <li>
                            <h2>%s</h2><br><h5>%s %s</h5><br><h3>%s</h3><br><h4 id='section'>%s</h4>
                            
                        </li>
    
        </div>
        ", htmlspecialchars($row["story_title"]),htmlspecialchars($row["story_username"]),htmlspecialchars($row["datetime"]),htmlspecialchars($row["story_description"]),htmlspecialchars($row["story_contents"]));
}
echo "</ul>\n";
echo "<br><hr>";

echo "<h2>Comments</h2>";
$stmt = $mysqli->prepare("select comment_id, comment,c.datetime,u.username from comments c join storys s on (c.story_id=s.story_id and s.story_id=?) join users u on (u.user_id=c.user_id) group by c.datetime DESC;");
if(!$stmt) {
    	printf("Query Prep For My Stories Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->bind_param('i',$story_id);

$stmt->execute();

$result = $stmt->get_result();

echo "<ul>\n";
while($row = $result->fetch_assoc()){
		
        printf("
            <div id='section'>
                        <li>
                            <h3 id='users'>%s</h3><h3> %s</h3><br><h4>%s</h4><br>
                            <hr>    
                        </li>    
        </div>
        ", htmlspecialchars($row['username']),htmlspecialchars($row['datetime']),htmlspecialchars($row['comment']));

} 


printf("
<form action='add_comment.php'>
   <label> Add comment:<br><textarea class = 'commentBlock' name='add_comment'></textarea></label><br><br>
   <input type='hidden' name='story_id' value='%s'>
   <input type='submit' name='submit' value='submit'>
</form>",$story_id);
$stmt->close();
 ?>
 
<div id="footer">
Copyright @Bowen She 437748
</div>

 

</html>