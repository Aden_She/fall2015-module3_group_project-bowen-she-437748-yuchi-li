<?php
session_start();
$username=$_POST['username'];
$insecure_1=$_POST['secure_1'];
$insecure_2=$_POST['secure_2'];

require "storyPavilion.php";

$stmt=$mysqli->prepare("select count(*),secure_1,secure_2 from users where user_name=?");
//echo $username;
if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->bind_param('s',$username);
$stmt->execute();

$stmt->bind_result($cnt,$secure_1,$secure_2);
$stmt->fetch();

//echo $cnt;
//echo $secure_1;
//echo $secure_2;

if($cnt==1&&htmlspecialchars($secure_1)==htmlspecialchars($insecure_1)&&htmlspecialchars($secure_2)==htmlspecialchars($insecure_2)){
    header("Location: resetpwd.html");
    $_SESSION['username']=$username;
    exit;
}

else{
    echo "User not exist or secure questions don't match, try again.";
    echo '<form action="pwd_retrieve.php">
            <input type="submit" value="back">
        </form>';
}

?>