<!DOCTYPE html>
    <html lang="en">
        <head>
            <title>Welcome to the register page!</title>
            
            <style type="text/css">
                h2{
                    color:red;
                    font-style:italic;
                }
            </style>
        </head>
        
        <body>
            <h1>Register</h1><br>
            
            <?php
            session_start();
            if ($_SESSION['pwd_match']==1){
                echo "<h2>The password you entered doesn't match, try again.</h2>";
            }
            
            if($_SESSION['usr_match']==1){
                echo "<h2>The username you entered already exists.</h2>";
            }
            ?><br>
            
            <form action="registerCheck.php" method="post">
                <p>
                    <label for="username">Input username: </label>
                    <input type="text" name="username" id="username" /><br><br>
                    
                    <label for="password">Input password: </label>
                    <input type="text" name="password" id="password" /><br><br>
                    
                    <label for="re_password">Re-input password: </label>
                    <input type="text" name="re_password" id="re_password" /><br><br>
           
                    <label for="secure_1">Secure question 1: Where did you born?</label><br>
                    <input type="text" name="secure_1" id="secure_1" /><br><br>
                    
                    <label for="secure_2">Secure question 2: What is your favourate food?</label><br>
                    <input type="text" name="secure_2" id="secure_2" /><br><br>
         
                    <input type="submit" value="submit" />
                    <input type="reset" />
                </p>
            </form>
        </body>
    </html>