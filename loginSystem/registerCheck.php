<?php

session_start();
$_SESSIONS['pwd_match']=0;
$_SESSION['usr_match']=0;
$password=$_POST['password'];
$re_password=$_POST['re_password'];
$secure_1=$_POST['secure_1'];
$secure_2=$_POST['secure_2'];

if(htmlentities($password)!=htmlentities($re_password)){
    $_SESSION['pwd_match']=1;
    header("Location: register.php");
    exit;
}

require '../storyPavilion_database.php';

$stmt=$mysqli->prepare("select username from users");
if(!$stmt){
    printf("Qurey Prep Failed: %s\n", $mysqli->error);
    exit;
}

$stmt->execute();

$stmt->bind_result($reg_username);

//echo "<ul>\n";
while ($stmt->fetch()){
    if(htmlspecialchars($reg_username)==htmlspecialchars($_POST['username'])){
        $_SESSION['usr_match']=1;
        header("Location: register.php");
        exit;
    }
}

if ( isset( $_POST['username'] ) ){
$username = strip_tags( trim( $_POST['username'] ) );
}

if ( isset( $_POST['password'] ) ){
$crypt_password=crypt(strip_tags( trim( $password)));
}

$stmt=$mysqli->prepare("insert into users (user_id, username, user_password,secure_1,secure_2) values (' ',?,?,?,?)");
if(!$stmt){
    printf("Query Prep For Register Failed: %s\n", $mysqli->error);
    exit;
}

$stmt->bind_param('ssss',$username,$crypt_password,$secure_1,$secure_2);

$stmt->execute();

$stmt->close();

echo htmlentities("Register complete!");
echo "<ul>\n";
echo "<a href=login.html>Ready to login?</a>";
?>